<?php

namespace App\Http\Controllers;

use App\Models\pdevToken;
use App\Models\pdevUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $rc  = 200;
        $msg = "Success Login";

        $username = $request->username;
        $password = $request->password;
        
        $pdev_user = pdevUser::where('username', $username)->first();

        if ($pdev_user) {
            if (Hash::check($password, $pdev_user->password)) {
                
                $generate_token = $this->generateToken($pdev_user->id, $username, $password);

                $res = [
                    'expired' => $generate_token['expired_time'],
                    'token'   => $generate_token['token']
                ];

            }else{
                $rc  = 401;
                $msg = 'Password salah';
                $res = [];
            }
        }else{
            $rc = 401;
            $msg = 'User tidak ditemukan';
            $res = [];
        }

        $data = [
            'rc'   => $rc,
            'msg'  => $msg,
            'data' => $res
        ];

        return response()->json($data);
    }

    public function register(Request $request)
    {
        $username = $request->username;
        $password = $request->password;

        // check users
        $check = pdevUser::where('username', $username)->first();
        if ($check) {
            $data = [
                'rc' => 409,
                'msg' => "Username sudah ada, gunakkan username lain!",
                'data' => []
            ];
        }else{
            $pdev_user = new pdevUser();
            $pdev_user->username = $request->username;
            $pdev_user->password = bcrypt($request->password);
            $pdev_user->save();
    
            $generate_token = $this->generateToken($pdev_user->id, $username, $password);
    
            $data = [
                'rc' => 200,
                'msg' => 'Berhasil registrasi',
                'data' => [
                    'expired' => $generate_token['expired_time'],
                    'token'   => $generate_token['token']
                ]
            ];
        }
        

        return response()->json($data);
    }

    public function generateToken($user_id, $username, $password)
    {
        $token = bcrypt($username.$password);
        $token = "pdev-auth-".str_replace('$','',str_replace('.','',$token));

        // expired time
        $currentDateTime = Carbon::now();
        $updatedDateTime = $currentDateTime->addHours(2);

        // Format the updated date and time
        $expired_time = $updatedDateTime->format('Y-m-d H:i:s');

        $pdev_token          = new pdevToken();
        $pdev_token->user_id = $user_id;
        $pdev_token->value   = $token;
        $pdev_token->expired = $expired_time;
        $pdev_token->save();

        return [
            'expired_time' => $expired_time,
            'token' => $token
        ];
    }

    public function check(Request $request)
    {
        $pdev_token = pdevToken::where('value', $request->token)->orderBy('id','DESC')->first();
        if ($pdev_token) {
            $now = date('Y-m-d H:i:s');
            $expired = $pdev_token->expired;

            if ($now > $expired) {
                $res = [
                    'rc' => 498,
                    'msg' => 'Token Expired'
                ];
            }else{
                $res = [
                    'rc' => 200,
                    'msg' => 'Token Active',
                    'expired' => $expired
                ];
            }

        }else{
            $res = [
                'rc' => 401,
                'msg' => 'Unregistered / Token not found'
            ];
        }

        return response()->json($res);
    }
}
